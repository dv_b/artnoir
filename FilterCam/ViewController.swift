//
//  ViewController.swift
//  FilterCam
//
//  Created by bulat davlyatshin on 10/7/16.
//  Copyright © 2016 bulat davlyatshin. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion
import Photos

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let imagePicker = UIImagePickerController()
    
    let filter = CIFilter(name: "CIPhotoEffectNoir")!
    
    var currentImageIndex = 0
    var imageNum = 16
    var images = ["DOVA6482.jpg", "2-3.png", "3-1.png", "4.png", "7-1.png", "8.png", "5.png", "6.png", "7-1.png", "9.png", "10.png", "11.png", "12.png", "13.png", "14 - new.png", "15 - new.png"]
    var isFreezed = false
    var fromGallery = false
    var isFrontCamera = true {
        didSet {
            loadCamera()
        }
    }
    
    
    var foregroundImage = UIImageView()
    
    @IBOutlet weak var foreground: UIImageView!
    
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    lazy var manager = CMMotionManager()
    
    var image: UIImage? {
        get {
            return foregroundImage.image
        }
        set {
            foregroundImage.image = newValue
            foregroundImage.sizeToFit()
            scrollView?.contentSize = foregroundImage.bounds.size
            let randY = CGFloat(arc4random_uniform(1500) + 1)
            scrollView.contentOffset.y = randY
//            scrollView.scrollRectToVisible(CGRect(x: Int(scrollView.contentOffset.x), y: randY, width: Int(foregroundImage.bounds.size.width), height: Int(foregroundImage.bounds.size.height)), animated: true)
        }
    }
    
    var galleryImage: UIImage?
    
    var resultImage: UIImage?
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var galleryModeMenuBar: UIView!
    
    @IBAction func galleryModeClose(sender: UIButton) {
        setInitialVisibility()
        loadCamera()
        startAccelerometerUpdates()
    }
    
    @IBAction func galleryModeNext(sender: UIButton) {
        currentImageIndex = (currentImageIndex + 1) % imageNum
        image = UIImage(named: images[currentImageIndex])
    }
    
    @IBAction func galleryModeSave(sender: UIButton) {
        if let image = mergeImage(bgImage.image!, topImage: getForegroundFrame()!) {
            resultImage = image
            showShareOptionMenu()
        }
    }
    
    @IBAction func switchCamera(sender: UIButton) {
        isFrontCamera = !isFrontCamera
        
    }
    
    @IBOutlet weak var cameraSwitchBtn: UIButton!
    
    @IBOutlet weak var saveLabel: UILabel!
    
    @IBOutlet weak var savedPopup: UIButton!
    
    @IBOutlet weak var shareMenuBar: UIView!
    
   
    @IBAction func instaShare(sender: AnyObject) {
        if let image = resultImage {
            share(image)
        }
    }
    
    @IBAction func backToCamera(sender: AnyObject) {
        defreeze()
    }
    
    
    let offset: Double = 0.005
    
    var prevX: Double = 0.0
    var prevY: Double = 0.0
    
    var currentX: Double = 0.0
    var currentY: Double = 0.0
    
    var currentRawX: Double = 0.0
    var currentRawY: Double = 0.0
    
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    
    var screenWidth: Int?
    var screenHeight: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.addSubview(foregroundImage)
        scrollView.delegate = self
        image = UIImage(named: images[currentImageIndex])!
        
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        imagePicker.delegate = self
        setInitialVisibility()
        
        //    scrollView.decelerationRate = UIScrollViewDecelerationRateFast
        //    scrollView.scrollRectToVisible(CGRect(x: foregroundImage.frame.size.width / 2, y: foregroundImage.frame.size.height / 2, width: 1, height: 1), animated: true)
        
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            bgImage.contentMode = .ScaleAspectFit
            bgImage.image = pickedImage
            galleryImage = pickedImage
            fromGallery = true
            setGalleryMode()
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
//        defreeze()
    }
    
    private func getForegroundFrame() -> UIImage? {
        var x = currentX * Double(scrollView.contentSize.width) / 0.6
        var y = currentY * Double(scrollView.contentSize.height) / 1.1
        
        if x >= Double(scrollView.contentSize.width) {
            x = Double(scrollView.contentSize.width) - 1
        }
        
        if x == 0 {
            x = 1
        }
        
        
        if y >= Double(scrollView.contentSize.height) {
            y = Double(scrollView.contentSize.height) - 1
        }
        
        if y == 0.0 {
            y = 1.0
        }

        guard let image = CGImageCreateWithImageInRect((foregroundImage.image?.CGImage)!,
                                                       CGRect(origin: CGPoint(x: x, y: y),
                                                        size: bgImage.image!.size))
            else {
                return nil
        }
        return UIImage(CGImage: image)
    }
    
    func setInitialVisibility() {
        savedPopup.hidden = true
        saveLabel.hidden = true
        shareMenuBar.hidden = true
        galleryModeMenuBar.hidden = true
        captureBtn.hidden = false
        cameraSwitchBtn.hidden = false
        acceptBtn.hidden = false
        cancelBtn.hidden = false
    }
    
    func setGalleryMode() {
        galleryModeMenuBar.hidden = false
        shareMenuBar.hidden = true
        captureBtn.hidden = true
        cameraSwitchBtn.hidden = true
        acceptBtn.hidden = true
        cancelBtn.hidden = true
    }
    
    func showSavedState() {
        savedPopup.hidden = false
        saveLabel.hidden = false
        let time = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64(NSEC_PER_SEC))
        dispatch_after(time, dispatch_get_main_queue()) {
            // Put your code which should be executed with a delay here
            self.savedPopup.hidden = true
            self.saveLabel.hidden = true
        }
        
    }
    
    func showShareOptionMenu() {
        shareMenuBar.hidden = false
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
//        scrollView.contentSize.width = CGFloat(screenWidth!)
//        scrollView.scrollEnabled = true
        manager.stopAccelerometerUpdates()
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        scrollView.scrollEnabled = false
//        scrollView.contentSize.width = foregroundImage.bounds.width
        if !isFreezed && !fromGallery {
            startAccelerometerUpdates()
        }
    }
    
    private func mergeImage(bottomImage: UIImage, topImage: UIImage) -> UIImage? {
        let size = CGSize(width: bottomImage.size.width, height: bottomImage.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        print("\(topImage.size.height)")
        let bottomArea = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
        bottomImage.drawInRect(bottomArea)
        
        topImage.drawInRect(bottomArea, blendMode: .Normal, alpha: 0.8)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if !fromGallery {
            loadCamera()
        } else {
//            session?.stopRunning()
            
            dispatch_async(dispatch_get_main_queue()) {
                let im = CIImage(image: self.galleryImage!)
                self.filter.setValue(im, forKey: kCIInputImageKey)
                
                let filteredImage = UIImage(CIImage: self.filter.valueForKey(kCIOutputImageKey) as! CIImage!, scale: 1.0, orientation: UIImageOrientation.UpMirrored)
                self.bgImage.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                let myLayer = CALayer()
                let myImage = filteredImage.CGImage
                myLayer.frame = self.bgImage.bounds
                myLayer.contents = myImage
                self.bgImage.layer.addSublayer(myLayer)
            }
        }
        
    }
    
    var frontCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        
        connection.videoOrientation = AVCaptureVideoOrientation.Portrait
        
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        let cameraImage = CIImage(CVPixelBuffer: pixelBuffer!)
        
        filter.setValue(cameraImage, forKey: kCIInputImageKey)
        
        
        let filteredImage = UIImage(CIImage: filter.valueForKey(kCIOutputImageKey) as! CIImage!, scale: 1.0, orientation: UIImageOrientation.Up)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.bgImage.image = filteredImage
        }
        
    }
    
    func getFrontCamera() -> AVCaptureDevice?{
        let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        
        
        for device in videoDevices!{
            let device = device as! AVCaptureDevice
            if device.position == AVCaptureDevicePosition.Front {
                return device
            }
        }
        return nil
    }
    
    func getBackCamera() -> AVCaptureDevice? {
        let videoDevices =  AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in videoDevices!{
            let device = device as! AVCaptureDevice
            if device.position == AVCaptureDevicePosition.Back {
                return device
            }
        }
        return nil
    }
    
    func loadCamera() {
        bgImage.image = nil
        if(session == nil){
            session = AVCaptureSession()
            session!.sessionPreset = AVCaptureSessionPresetPhoto
        }
        var error: NSError?
        var input: AVCaptureDeviceInput!
        
        frontCamera = (isFrontCamera ? getFrontCamera() : getBackCamera())
        
        do {
            input = try AVCaptureDeviceInput(device: frontCamera)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        for i : AVCaptureDeviceInput in (self.session?.inputs as! [AVCaptureDeviceInput]){
            self.session?.removeInput(i)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            for j: AVCaptureStillImageOutput in session?.outputs as! [AVCaptureStillImageOutput] {
                session?.removeOutput(j)
            }
            
//            session?.removeOutput(stillImageOutput)
            if session!.canAddOutput(stillImageOutput) {
                session!.addOutput(stillImageOutput)
                if videoPreviewLayer == nil {
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                    
                }
//                self.bgImage.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                self.bgImage.layer.addSublayer(videoPreviewLayer!)
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.session!.startRunning()
                }
            }
        }
    }
    
    
    func share(image: UIImage) {
        let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
        presentViewController(vc, animated: true, completion: nil)
        vc.completionWithItemsHandler = {[weak self] (activityType, completed:Bool, returnedItems:[AnyObject]?, error: NSError?) in
            if (!completed) {
                return
            }
            self?.showSavedState()
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        videoPreviewLayer!.frame = (view?.bounds)!
        if !fromGallery {
            startAccelerometerUpdates()
        }
        
    }
    
    func startAccelerometerUpdates() {
        if manager.accelerometerAvailable {
            manager.accelerometerUpdateInterval = 0.1;
            manager.startAccelerometerUpdatesToQueue(NSOperationQueue(), withHandler: {
                [weak self] (data, error) in
                if let acceleration = data?.acceleration {
                    
                    if acceleration.x < -0.3 {
                        self!.currentX = 0.6
                    } else if acceleration.x > 0.3 {
                        self!.currentX = 0.0
                    } else {
                        self!.currentX = -1.0 * acceleration.x + 0.3
                    }
                    
                    if acceleration.y < -1.0 {
                        self!.currentY = 0.0
                    } else if acceleration.y > 0.1 {
                        self!.currentY = 1.1
                    } else {
                        self!.currentY = acceleration.y + 1.0
                    }
                    
                    
                    var x = self!.currentX * Double((self?.scrollView.contentSize.width)!) / 0.6
                    var y = self!.currentY * Double((self?.scrollView.contentSize.height)!) / 1.1
                    
                    if x >= Double((self?.scrollView.contentSize.width)!) {
                        x = Double((self?.scrollView.contentSize.width)!) - 1
                    }
                    
                    if x == 0 {
                        x = 1
                    }
                    
                    if y >= Double((self?.scrollView.contentSize.height)!) {
                        y = Double((self?.scrollView.contentSize.height)!) - 1
                    }
                    
                    if y == 0.0 {
                        y = 1.0
                    }
//                    if abs(self!.currentX - self!.prevX) > 0.0 || abs(self!.currentY - self!.prevY) > 0.0 {
                        self?.scrollView.scrollRectToVisible(CGRect(
                            x: Int(x),
//                            y: Int(y),
                            y: Int(self!.scrollView.contentOffset.y),
                            width: self!.screenWidth!,
                            height: self!.screenHeight!),
                            animated: true)
//                    }
                    
                    self!.prevX = self!.currentX
                    self!.prevY = self!.currentY
//                    print(x, y)
                    
                }
                })
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        manager.stopAccelerometerUpdates()
    }
    
    @IBOutlet weak var acceptBtn: UIButton!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBAction func onLeftBtnClick(sender: UIButton) {
        if isFreezed {
            defreeze()
        } else {
            startImagePicker()
//            pickMode()
        }
    }
    
    @IBAction func onRightBtnClick(sender: UIButton) {
        if isFreezed {
            if let image = mergeImage(bgImage.image!, topImage: getForegroundFrame()!) {
                resultImage = image
                showShareOptionMenu()
            }
        } else {
            currentImageIndex = (currentImageIndex + 1) % imageNum
            image = UIImage(named: images[currentImageIndex])
        }
    }
    
    func pickMode() {
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Камера", style: .Default, handler: {
            [weak self] (alert: UIAlertAction!) -> Void in
            //
            self?.loadCamera()
        })
        let saveAction = UIAlertAction(title: "Галерея", style: .Default, handler: {
            [weak self] (alert: UIAlertAction!) -> Void in
            //
            self?.startImagePicker()
            
        })
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
//        self.present(optionMenu, animated: true, completion: nil)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func defreeze() {
        isFreezed = false
        if !fromGallery {
            session!.startRunning()
            cameraSwitchBtn.hidden = false
            captureBtn.hidden = false
            startAccelerometerUpdates()
        }
        
        shareMenuBar.hidden = true
//        scrollView.scrollEnabled = true
        acceptBtn.setBackgroundImage(UIImage(named: "choose_monotype.png"), forState: .Normal)
        cancelBtn.setBackgroundImage(UIImage(named: "choose_photo.png"), forState: .Normal)
//        scrollView.contentSize.width = foreground.bounds.width
    }
    
    func freeze() {
        session!.stopRunning()
        manager.stopAccelerometerUpdates()
        acceptBtn.setBackgroundImage(UIImage(named: "done.png"), forState: .Normal)
        cancelBtn.setBackgroundImage(UIImage(named: "back.png"), forState: .Normal)
        isFreezed = true
        captureBtn.hidden = true
        cameraSwitchBtn.hidden = true
        //        scrollView.scrollEnabled = true
        //        scrollView.contentSize.width = CGFloat(screenWidth!)
    }
    
    @IBOutlet weak var captureBtn: UIButton!
    
    @IBAction func freezeCamera(sender: AnyObject) {
        freeze()
    }
    
    @IBAction func didTakePhoto(sender: UIButton) {
        session!.stopRunning()
        manager.stopAccelerometerUpdates()
    }
    
    func startImagePicker() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
}

